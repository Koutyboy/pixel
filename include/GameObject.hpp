#pragma once

#include <SFML/Graphics.hpp>

class GameObject
{
protected:
	float m_x, m_y;
	bool m_deleteObject;
	sf::Sprite m_sprite;
public:
	GameObject() { m_deleteObject = false; }
	virtual ~GameObject() {};
	bool markedForRemoval() { return m_deleteObject; }
	virtual void draw(sf::RenderWindow& window) {
		m_sprite.setPosition(m_x,m_y);
		window.draw(m_sprite);
	};
	virtual void update(float dt, float time)=0;
};
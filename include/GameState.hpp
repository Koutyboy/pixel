#pragma once

namespace sf { class RenderWindow; class Event; }
#include <memory>

class GameState
{
protected:
	bool m_done;
	std::shared_ptr<GameState> m_nextState;
public:
	GameState() { m_done = false; }
	virtual ~GameState() {};
	bool isDone() { return m_done; }
	std::shared_ptr<GameState>& getNextState() { return m_nextState; }
	
	virtual void draw(sf::RenderWindow& window)=0;
	virtual void update(float dt, float time)=0;
	virtual void handleEvent(sf::Event& event)=0;
};
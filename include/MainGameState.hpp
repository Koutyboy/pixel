#pragma once

namespace sf { class RenderWindow; class Event; }

#include <iostream>
#include <vector>
#include <memory>

#include "GameState.hpp"
#include "GameObject.hpp"
#include "Player.hpp"

class MainGameState : public GameState
{
private:
	Player player;
	std::vector< std::shared_ptr< GameObject > > m_objects;
public:
	~MainGameState() { std::cout << "MainGameState was destroyed" << std::endl; }
	void handleEvent(sf::Event& event);
	void update(float dt, float time);
	void draw(sf::RenderWindow& window);
};
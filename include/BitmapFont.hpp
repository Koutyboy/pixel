#pragma once

#include <SFML/Graphics.hpp>

struct BitString
{
	std::string text;
	float x;
	float y;
	BitString(std::string text, float x, float y) {
		this->text = text;
		this->x = x;
		this->y = y;
	}
};

class BitmapFont
{
private:
	int m_MinRange, m_MaxRange;
	sf::Texture m_texture;
	sf::Sprite m_sprite;
	std::vector< sf::IntRect > m_rectangles;
public:
	BitmapFont();
	void draw(sf::RenderWindow& window, const BitString& bs);
};
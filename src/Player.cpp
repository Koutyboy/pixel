#include "Player.hpp"
#include <iostream>

Player::Player() {
	m_texture.loadFromFile("data/player.png");
	m_sprite.setTexture(m_texture);
	m_x = 100;
	m_y = 100;
	m_speed = 600;
}

void Player::update(float dt, float time) {
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::Left ) ) {
		m_x -= m_speed * dt;
	}
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::Right ) ) {
		m_x += m_speed * dt;
	}
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::Up ) ) {
		m_y -= m_speed * dt;
	}
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::Down ) ) {
		m_y += m_speed * dt;
	}
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::Space )) {
	}
}
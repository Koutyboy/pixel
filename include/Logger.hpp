#pragma once

#include <iostream>
#include <string>
#ifdef _WIN32
#include <windows.h>
#endif

class Logger
{
public:
	static void msgbox(std::string title, std::string msg) {
		#ifdef _WIN32
		MessageBox(NULL,
		msg.c_str(),
		title.c_str(),
		MB_OK | MB_ICONINFORMATION );
		#else
		std::cout << "["+title+"] " << msg << std::endl;
		#endif
	}
};
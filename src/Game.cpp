#include "Game.hpp"
#include "MainGameState.hpp"
#include <iostream>

int Game::exec() {
	initialize();
	float framerate = 1.0f / 60.0f;
	float lastframe, nextframe;
	
	sf::Clock clock;
	while(m_window.isOpen() && !m_stateStack.empty() ) {
		handleState();
		handleEvent();
		if(nextframe <= clock.getElapsedTime().asSeconds()) {
			float dt = clock.getElapsedTime().asSeconds() - lastframe;
			lastframe = clock.getElapsedTime().asSeconds();
			update(dt,lastframe);
			nextframe += framerate;
		}
		draw();
	}
	return 0;
}

bool Game::initialize() {
	m_window.create(sf::VideoMode(600,400,32), "@Pixel" );
	m_stateStack.push( std::shared_ptr<GameState>( new MainGameState() ) );

	bittext.assign("Pixel - SHMUP!");
	bittext.append("\nThis is a new line!");
	bittext.append("\nAs long as appending-calls isn't huge or many");
	bittext.append("\nIt's fine to use it!\nAnd it finds newlines in strings!");
	
	return true;
}

void Game::handleEvent() {
	while(m_window.pollEvent(m_event)) {
		if( !m_stateStack.empty() ) {
			m_stateStack.top()->handleEvent(m_event);
		}
	}
}

void Game::handleState() {
	if(!m_stateStack.empty()) {
		if (m_stateStack.top()->isDone()) {
			std::shared_ptr<GameState> state = m_stateStack.top()->getNextState();
			m_stateStack.pop();
			if(state.get() != nullptr) {
				m_stateStack.push(state);
			}
		}
	}
}
void Game::update(float dt, float time) {
	if( !m_stateStack.empty() ) {
		m_stateStack.top()->update(dt,time);
	}
	
	if( sf::Keyboard::isKeyPressed( sf::Keyboard::H ) )
		bittext.assign("H YOU PRESSED!");
	
	float x = sf::Mouse::getPosition(m_window).x;
	float y = sf::Mouse::getPosition(m_window).y;
	bittext.setPosition(x,y);
}

void Game::draw() {
	m_window.clear( sf::Color::Black );
	if( !m_stateStack.empty() ) {
		m_stateStack.top()->draw(m_window);
	}
	bittext.draw(m_window);
	m_window.display();
}
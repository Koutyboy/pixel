#pragma once


#include "GameObject.hpp"


class Player : public GameObject
{
private:
	int m_health;
	float m_speed, m_firerate, m_damage;
	sf::Texture m_texture;
	
	void handleMovement();
public:
	Player();
	void update(float dt, float time);
};
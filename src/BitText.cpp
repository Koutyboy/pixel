#include "BitText.hpp"
#include <sstream>

BitmapFont BitText::_bitmapfont;

BitText::BitText() {
	build("",0,0);
}

BitText::BitText(std::string text, float x, float y) {
	build(text,x,y);
}

std::vector< std::string > BitText::makeSubStr(std::string text) {
	std::vector<std::string> tokens;
	std::istringstream is(text);
	std::string tmp;
	while( std::getline(is, tmp, '\n') ) tokens.push_back(tmp);
	return tokens;
}

void BitText::draw(sf::RenderWindow& window) {
	for( BitString bs : m_lines ) BitText::_bitmapfont.draw(window,bs);
}

void BitText::build(std::string text, float x, float y) {
	m_lines.clear();
	if(text != "") {
		auto lines = makeSubStr(text);
		for( int i = 0; i < lines.size(); i++ ) {
			BitString bs(lines[i],x,y+i*8);
			m_lines.push_back(bs);
		}
	}
	else {
		BitString bs("",x,y);
		m_lines.push_back(bs);
	}
}

void BitText::assign(std::string text) {
	float x = m_lines[0].x;
	float y = m_lines[0].y;
	build(text,x,y);
}

void BitText::append(std::string text) {
	int end = m_lines.size()-1;
	float x = m_lines[end].x;
	float y = m_lines[end].y;
	std::string newstr = m_lines[end].text + text;
	m_lines.erase(m_lines.begin()+end);
	auto lines = makeSubStr(newstr);
	for( int i = 0; i < lines.size(); i++ ) {
		BitString bs(lines[i],x,y+i*8);
		m_lines.push_back(bs);
	}
}

void BitText::setPosition(float x, float y) {
	for( int i = 0; i < m_lines.size(); i++ ) {
		m_lines[i].x = x;
		m_lines[i].y = y + i * 8;
	}
}

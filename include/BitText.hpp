#pragma once

#include "BitmapFont.hpp"

class BitText
{
private:
	static BitmapFont _bitmapfont;
	std::vector< BitString > m_lines;
	
	std::vector< std::string > makeSubStr(std::string text);
	
	void build(std::string text, float x, float y);
public:
	BitText();
	BitText(std::string text, float x, float y);
	void draw(sf::RenderWindow& window);
	
	void setPosition(float x, float y);
	void append(std::string text);
	void assign(std::string text);
};
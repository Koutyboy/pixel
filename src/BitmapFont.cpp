#include "BitmapFont.hpp"

BitmapFont::BitmapFont() {
	m_texture.loadFromFile("data/font.png");
	m_MinRange = 32;
	m_MaxRange = 126;
	int numberOfLetters = m_MaxRange - m_MinRange;
	m_rectangles.reserve(numberOfLetters);
	for(int i = 0; i < numberOfLetters; i++ ) {
		sf::IntRect rect(i*8,0,8,8);
		m_rectangles.push_back(rect);
	}
	m_sprite.setTexture(m_texture);
}

void BitmapFont::draw(sf::RenderWindow& window, const BitString& bs) {
	for( int i = 0; i < bs.text.size(); i++ ) {
		int ascii = static_cast<int>(bs.text[i]);
		m_sprite.setTextureRect(m_rectangles[ascii-m_MinRange]);
		m_sprite.setPosition(bs.x + 8 * i,bs.y);
		window.draw(m_sprite);
	}
}

#pragma once

#include <SFML/Graphics.hpp>
#include <stack>
#include <memory>

#include "GameState.hpp"
#include "BitText.hpp"

class Game
{
private:
	sf::RenderWindow m_window;
	sf::Event m_event;
	
	std::stack< std::shared_ptr<GameState> > m_stateStack;
	BitText bittext;
	
	bool initialize();
	void handleEvent();
	void handleState();
	void update(float dt, float time);
	void draw();
public:
	int exec();
};
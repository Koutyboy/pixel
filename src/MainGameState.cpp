#include "MainGameState.hpp"
#include <SFML/Graphics.hpp>
void MainGameState::handleEvent(sf::Event& event) {
	if( sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		m_done = true;
}
void MainGameState::update(float dt, float time) {
	for(auto object : m_objects) object->update(dt,time);
	for(auto it = m_objects.begin(); it != m_objects.end();++it)
		if( (*it)->markedForRemoval() ) it = m_objects.erase(it);
	player.update(dt, time);
}
void MainGameState::draw(sf::RenderWindow& window) {
	for(auto object : m_objects) object->draw(window);
	player.draw(window);
}